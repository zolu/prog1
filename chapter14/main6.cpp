#include <iostream>
#include <string>

int main()
{
	class B1 {
		public:
		virtual void vf(){
			std::cout << "B1::vf()\n";
		};
		void f(){
			std::cout << "B1::f()\n";
		};
		virtual void pvf()=0;
	};

	class D1 : public B1 {
		public:
		void vf() override{
			std::cout << "D1::vf()\n"; 
		}
		void pvf() override{
			std::cout << "D1::pvf()\n"; 
		}
		void f(){
			std::cout << "D1::f()\n"; 
		}
	};
	class D2 : public D1 {
		public:
		void pvf() override{
			std::cout << "D2::pvf()\n"; 
		}
	};
	class B2 {
		public:
		virtual void pvf()=0;
	};
	class D21 : public B2{
		public:
		std::string xd;
		void pvf() override{
			std::cout << xd << std::endl;
		}
	};

	class D22 : public B2{
		public:
		int xdd;
		void pvf() override{
			std::cout << xdd << std::endl;
		}
		void f(B2& b2ref){
			b2ref.pvf();
	}
	};

	D1 d1;
	d1.vf();
	d1.pvf();
	d1.f();
	
	D2 d2;
	d2.vf();
	d2.pvf();
	d2.f();

	D21 d21;
	D22 d22;

	d21.xd="xdd";
	d22.xdd=8;

	d22.f(d21);
	d22.f(d22);

	return 0;
}
