#include <iostream>

int main()
{
	class B1 {
		public:
		virtual void vf(){
			std::cout << "B1::vf()\n";
		};
		void f(){
			std::cout << "B1::f()\n";
		};
	};

	B1 b1;
	b1.vf();
	b1.f();

	class D1 : public B1 {
		public:
		void vf() override{
			std::cout << "D1::vf()\n"; 
		}
		void f(){
			std::cout << "D1::f()\n"; 
		}
	};

	D1 d1;
	d1.vf();
	d1.f();

	B1& refb1 = d1;
	refb1.vf();
	refb1.f();

	return 0;
}
