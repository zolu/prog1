/*
    g++ main.cpp Graph.cpp Window.cpp GUI.cpp Simple_window.cpp -o main `fltk-config --ldflags --use-images` -std=c++11
*/
#include "Simple_window.h"
#include "Graph.h"

int main()
{
	try{
	Point tl {100,100};
	int width = 600;
	int height = 400;

	Simple_window win{tl, width, height, "Canvasxd"};

	Function sine {sin, 0,100, Point{20,150},1000,50,50};
	sine.set_color(Color::green);

	Axis xa {Axis::x, Point{20,300},300,12,"x axis"};
	Axis ya {Axis::y, Point{20,300},300,12,"y axis"};
	ya.set_color(Color::cyan);
	ya.label.set_color(Color::dark_blue);

	Polygon poly;
	poly.add(Point{100,200});
	poly.add(Point{350,100});
	poly.add(Point{400,200});

	poly.set_color(Color::green);
	poly.set_style(Line_style(Line_style::dash,1));

	Rectangle r{Point{200,200},100,50};
	r.set_fill_color(Color::blue);

	Closed_polyline poly_rect;
	poly_rect.add(Point{100,50});
	poly_rect.add(Point{200,50});
	poly_rect.add(Point{200,50});
	poly_rect.add(Point{100,100});
	poly_rect.add(Point{75,75});
	poly_rect.set_style(Line_style(Line_style::dash,2));
	poly_rect.set_fill_color(Color::green);

	Text t{Point{150,150}, "Hello xd"};
	t.set_font(Graph_lib::Font::times_bold);
	t.set_font_size(20);

	Image ii{Point{100,40},"gyozike.jpeg"};

	Image cal {Point{225,225},"gyozike.jpeg"};
	Circle c {Point{100,200},25};
	Ellipse e {Point{100,200},75,15};
	e.set_color(Color::dark_green);
	Mark m{Point{100,200},'x'};
	ostringstream oss;
	oss << "screen size:" << x_max() << "*" << y_max() << "; window size: "<< win.x_max() << "*" << win.y_max();
	Text sizes {Point{70,20},oss.str()};

	cal.set_mask(Point{40,40},200,150);
	win.attach(c);
	win.attach(m);
	win.attach(e);
	win.attach(sizes);
	win.attach(cal);
	win.attach(xa);
	win.attach(ya);
	win.attach(sine);
	win.attach(poly);
	win.attach(r);
	win.attach(poly_rect);
	win.attach(t);
	win.attach(ii);
	win.wait_for_button();

	ii.move(10,10);
	win.set_label("Canvas#10");

	win.wait_for_button();
	}
	catch (exception& e) {
		return 1;
	}
	catch (...) {
		return 2;
	}
}
