#include"std_lib_facilities.h"
string spCh = ";:\"'[]*&^%$#@!";
struct Person {
public:
	Person(){};
	Person(string f,string l, int a){

	if (f == "" or l == ""){
		error ("missing firstname or lastname");
	}

	for (int i = 0; i < f.length(); i++){
		for (int k = 0; k < spCh.length(); k++){
			if (f[i]==spCh[k]){
				error("name should not contain special characters.");
			}
		}
	}
	fname = f;

	for (int i = 0; i < l.length(); i++){
			for (int k = 0; k < spCh.length(); k++){
				if (l[i]==spCh[k]){
					error("name should not contain special characters.");
				}
			}
		}

	lname = l;
	if (a >= 0 && a <= 150){
		age = a;
	}else{
		error("age should be between 0 and 150");
	}
	};
	string getfName() const { return fname; }
	string getlName() const { return lname; }
	int getAge() const { return age; }


private:
	string fname;
	string lname;
	int age;
};

ostream& operator<<(ostream& os, const Person& p){
	cout << "Name: " << p.getfName() << " " << p.getlName() << "\nAge: " << p.getAge() << "\n";
	return os;
}
istream& operator>>(istream& is, Person& p){
	string f; string l; int a;
	cin >> f >> l >> a;
	p = Person(f,l,a);
	return is;
}

int main(){

	Person p1{"Goofy", "Gonzalez", 63};
	cout <<p1.getfName() << " " << p1.getlName() << " " <<p1.getAge() << endl;

	Person p2;
	cin >> p2;
	cout << p2;

	vector<Person> vpeople;


	//press ctrl z to end input
	for (Person t; cin >> t;){
		if (t.getfName()=="exit") break;
		vpeople.push_back(t);
	}

	cout << endl;
	for (int i = 0; i < vpeople.size(); i++){
		cout << vpeople[i] << endl;
	}

}
