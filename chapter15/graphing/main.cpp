/*
    g++ main.cpp Graph.cpp Window.cpp GUI.cpp Simple_window.cpp -o main `fltk-config --ldflags --use-images` -std=c++11
*/
#include "Simple_window.h"
#include "Graph.h"
double one(double x){return 1;}
double slope(double x){return x/2;}
double square(double x){return x*x;}
double slope_cos(double x){return slope(x) + cos(x);}
int main()
{
	try{
	Point tl {100,100};
	constexpr int width = 600;
	constexpr int height = 600;
	constexpr int axLength = 400;
	constexpr int offset = 100;
	constexpr int xscale = 20;
	constexpr int yscale = 20;
	constexpr int rmin = -10;
	constexpr int rmax = 11;
	constexpr int points = 400;
	std::string winTitle = "Function graphs";
	std::string axLabel = "1 == 20 pixels";

	Simple_window win{tl, width, height, winTitle};

	Axis xa {Axis::x, Point{offset,axLength-offset},axLength,axLength/xscale,axLabel};
	Axis ya {Axis::y, Point{axLength-offset,axLength+offset},axLength,axLength/yscale,axLabel};

	xa.set_color(Color::red);
	ya.set_color(Color::red);

	Function f1{one, rmin,rmax, Point{axLength-offset,axLength-offset},points,xscale,yscale};

	Function f2{slope, rmin,rmax, Point{axLength-offset,axLength-offset},points,xscale,yscale};

	Function f3{square, rmin,rmax, Point{axLength-offset,axLength-offset},points,xscale,yscale};

	Function f4{cos, rmin,rmax, Point{axLength-offset,axLength-offset},points,xscale,yscale};
	f4.set_color(Color::blue);

	Function f5{slope_cos, rmin,rmax, Point{axLength-offset,axLength-offset},points,xscale,yscale};
	
	Text tf2{Point{offset, axLength-10},"x/2"};

	win.attach(xa);
	win.attach(ya);
	win.attach(f1);
	win.attach(f2);
	win.attach(f3);
	win.attach(f4);
	win.attach(f5);
	win.attach(tf2);

	win.wait_for_button();
	}
	catch (exception& e) {
		return 1;
	}
	catch (...) {
		return 2;
	}
}
