#include <iostream>
#include <algorithm>
#include <array>
#include <vector>
#include <list>

template <typename T>
void print(const T& kako){
	for(auto& i : kako)
		std::cout << i << " " ;

	std::cout << std::endl;
}

template <typename T>
void inc(T& kako, int inci){
	for(auto& i : kako)
		i+=inci;	
}

template <typename Iter1, typename Iter2>
Iter2 wopy(Iter1 f1, Iter1 e1, Iter2 f2){
	while (f1  != e1){
		*f2=*f1;
		++f2;
		++f1;
	}
	return f2;
}

int main(){
	std::array <int,10> arr = {0,1,2,3,4,5,6,7,8,9};
	std::vector<int> vec = {0,1,2,3,4,5,6,7,8,9};
	std::list  <int> lis = {0,1,2,3,4,5,6,7,8,9};

	print(arr);
	print(vec);
	print(lis);
	std::cout << std::endl;

	std::array <int,10> arr2 = arr;
	std::vector<int> vec2 = vec;
	std::list  <int> lis2 = lis;

	inc(arr2,2);
	inc(vec2,3);
	inc(lis2,5);

	print(arr2);
	print(vec2);
	print(lis2);
	std::cout << std::endl;

	wopy(arr2.begin(),arr2.end(),vec2.begin());
	wopy(lis2.begin(),lis2.end(),arr2.begin());

	print(arr2);
	print(vec2);
	print(lis2);
	std::cout << std::endl;

	auto is3 = std::find(vec2.begin(),vec2.end(),3);

	if (is3 != vec2.end()){
		std::cout <<  distance(vec2.begin(), is3) << std::endl;
	}

	auto is27 = std::find(lis2.begin(),lis2.end(),3);

	if (is27 != lis2.end()){
		std::cout <<  distance(lis2.begin(), is27) << std::endl;
	}

	return 0;
}
