#include <iostream>
#include <cmath>
#include "include/Matrix.h"
#include <iomanip>
#include <complex>
using namespace std;
using namespace Numeric_lib;

void sq(int x){
	cin >> x;
	if (x < 0)
		cout << "no square root\n";
	else
		cout << sqrt(x) << endl;

}

int main(){

	cout << sizeof(char) << endl;
	cout << sizeof(short) << endl;
	cout << sizeof(int) << endl;
	cout << sizeof(long) << endl;
	cout << sizeof(float) << endl;
	cout << sizeof(double) << endl;
	cout << sizeof(int*) << endl;
	cout << sizeof(double*) << endl;

	Matrix<int> a(10);
	Matrix<int> b(100);
	Matrix<double> c(10);
	Matrix<int,2> d(10,10);
	Matrix<int,3> e(10,10,10);

	cout << sizeof(a) << endl;
	cout << sizeof(b) << endl;
	cout << sizeof(c) << endl;
	cout << sizeof(d) << endl;
	cout << sizeof(e) << endl;

	cout << a.size() << endl;
	cout << b.size() << endl;
	cout << c.size() << endl;
	cout << d.size() << endl;
	cout << e.size() << endl;

	cout << "enter 3 ints: \n";
	int z;
	for (int i = 0; i < 3; ++i){
		sq(z);
	}

	cout << "enter 10 doubles: \n";
	Matrix<double> dbl(10);
	for (int i=0; i<10;++i){
		cin >> dbl[i];
	}
	cout << "double matrix: \n";
	for (int i=0; i<10;++i){
		cout << dbl[i] << endl;
	}
    int n,m;
    cout << "Enter n, m: ";
    cin >> n >> m;

    Matrix<int,2> mTable(n,m);
		for (int i = 0; i < mTable.dim1(); i++){
			for (int j = 0; j < mTable.dim2(); j++){
				mTable[i][j]=(i+1)*(j+1);
				cout << setw(4) << mTable[i][j];
			}
			cout << endl;
		}

  cout << "Enter 10 complex numbers (Real,Imaginary):\n";
	Matrix<complex<double>> cmplx(3);
	for (int i = 0; i < cmplx.size(); ++i){
		cin >> cmplx[i];
	}

	complex<double> sum;
	for(int i = 0; i < cmplx.size(); ++i){
		sum+=cmplx[i];
	}
	cout << endl << "sum of complex nums: " << sum << endl;

	Matrix<int,2> ma(2,3);

	cout << "Enter 6 ints: " << endl;

	for(int i = 0; i < ma.dim1(); ++i){
		for (int j = 0; j < ma.dim2(); ++j){
			cin >> ma[i][j];
		}
	}
	for(int i = 0; i < ma.dim1(); ++i){
		for (int j = 0; j < ma.dim2(); ++j){
			cout << setw(3) << ma[i][j];
		}
		cout << endl;
	}

	return 0;
}
