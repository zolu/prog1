/*
    g++ main.cpp Graph.cpp Window.cpp GUI.cpp Simple_window.cpp -o main `fltk-config --ldflags --use-images` -std=c++11
*/
#include "Graph.h"
#include "Simple_window.h"

struct Lines_window : Graph_lib::Window {
	Lines_window(Point xy, int w, int h, const string& title);
	private:
		Open_polyline lines;

		Button next_button;
		Button quit_button;	
		In_box next_x;
		In_box next_y;
		Out_box xy_out;
		Menu color_menu;
		Button menu_button;

		Menu smenu;
		Button smenu_button;

		void change(Color c) {lines.set_color(c);}
		void schange(Line_style s) {lines.set_style(s);}

		void hide_menu() { color_menu.hide(); menu_button.show();}
		void hide_smenu() { smenu.hide(); smenu_button.show();}

		void red_pressed(){ change(Color::red); hide_menu();}
		void blue_pressed(){ change(Color::blue); hide_menu();}
		void black_pressed(){ change(Color::black); hide_menu();}

    void dot_pressed() { schange(Line_style::dot); hide_smenu(); }
    void dash_pressed() { schange(Line_style::dash); hide_smenu(); }
    void solid_pressed() { schange(Line_style::solid); hide_smenu(); }
    void dashdot_pressed() { schange(Line_style::dashdot); hide_smenu(); }
    void dashdotdot_pressed() { schange(Line_style::dashdotdot); hide_smenu(); }

		void menu_pressed(){ menu_button.hide(); color_menu.show();}
		void smenu_pressed(){ smenu_button.hide(); smenu.show();}

		void next(); 
		void quit(); 

		static void cb_red(Address,Address);
		static void cb_blue(Address,Address);
		static void cb_black(Address,Address);

		static void cb_dot(Address,Address);
		static void cb_dash(Address,Address);
		static void cb_solid(Address,Address);
		static void cb_dashdot(Address,Address);
		static void cb_dashdotdot(Address,Address);

		static void cb_menu(Address,Address);
		static void cb_smenu(Address,Address);

		static void cb_next(Address,Address);
		static void cb_quit(Address,Address);
};

Lines_window::Lines_window(Point xy, int w, int h, const string& title)
	:Window{xy,w,h,title},
	next_button{Point{x_max()-150,0}, 70, 20, "Next point",cb_next},
	quit_button{Point{x_max()-70,0}, 70, 20, "Quit",cb_quit},
	next_x{Point{x_max()-310,0}, 50, 20, "next x:"},
	next_y{Point{x_max()-210,0}, 50, 20, "next y:"},
	xy_out{Point{100,0}, 100, 20, "current (x,y):"},

	color_menu{Point{x_max()-70,30},70,20, Menu::vertical, "color"},
	menu_button{Point{x_max()-80,30}, 80, 20, "color menu", cb_menu},

	smenu{Point{x_max()-70,100},70,20, Menu::vertical, "color"},
	smenu_button{Point{x_max()-80,100}, 80, 20, "style menu", cb_smenu}

{
	attach(next_button);
	attach(quit_button);
	attach(next_x);
	attach(next_y);
	attach(xy_out);
	xy_out.put("no point");
	color_menu.attach(new Button{Point{0,0},0,0,"red",cb_red});
	color_menu.attach(new Button{Point{0,0},0,0,"blue",cb_blue});
	color_menu.attach(new Button{Point{0,0},0,0,"black",cb_black});

	smenu.attach(new Button{Point{0,0},0,0,"solid",cb_solid});
	smenu.attach(new Button{Point{0,0},0,0,"dot",cb_dot});
	smenu.attach(new Button{Point{0,0},0,0,"dash",cb_dash});
	smenu.attach(new Button{Point{0,0},0,0,"dashdot",cb_dashdot});
	smenu.attach(new Button{Point{0,0},0,0,"dashdotdot",cb_dashdotdot});

	attach(color_menu);
	attach(smenu);
	color_menu.hide();
	smenu.hide();
	attach(menu_button);
	attach(smenu_button);
	attach(lines);
}

void Lines_window::cb_quit(Address, Address pw)
{
	    reference_to<Lines_window>(pw).quit();
}

void Lines_window::cb_next(Address, Address pw)
{
	    reference_to<Lines_window>(pw).next();
}

void Lines_window::cb_menu(Address, Address pw)
{
	    reference_to<Lines_window>(pw).menu_pressed();
}

void Lines_window::cb_smenu(Address, Address pw)
{
	    reference_to<Lines_window>(pw).smenu_pressed();
}

void Lines_window::cb_blue(Address, Address pw)
{
	    reference_to<Lines_window>(pw).blue_pressed();
}

void Lines_window::cb_black(Address, Address pw)
{
	    reference_to<Lines_window>(pw).black_pressed();
}

void Lines_window::cb_red(Address, Address pw)
{
	    reference_to<Lines_window>(pw).red_pressed();
}

void Lines_window::cb_solid(Address, Address pw)
{
	    reference_to<Lines_window>(pw).solid_pressed();
}

void Lines_window::cb_dot(Address, Address pw)
{
	    reference_to<Lines_window>(pw).dot_pressed();
}

void Lines_window::cb_dash(Address, Address pw)
{
	    reference_to<Lines_window>(pw).dash_pressed();
}
void Lines_window::cb_dashdot(Address, Address pw)
{
	    reference_to<Lines_window>(pw).dashdot_pressed();
}
void Lines_window::cb_dashdotdot(Address, Address pw)
{
	    reference_to<Lines_window>(pw).dashdotdot_pressed();
}
void Lines_window::quit()
{
	hide();    
}

void Lines_window::next()
{
	int x = next_x.get_int();
	int y = next_y.get_int();
	lines.add(Point{x,y});
	ostringstream ss;
	ss << '(' << x << ',' << y << ')';
	xy_out.put(ss.str());
	redraw();
}

int main()
{
	try{
		Lines_window win {Point{100,100},600,400,"lines"};
		return gui_main();
	}
	catch (exception& e) {
		return 1;
	}
	catch (...) {
		return 2;
	}
}
