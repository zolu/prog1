#include <iostream>
#include <map>
#include <algorithm>
#include <string>
#include <numeric>
using namespace std;

ostream& operator<<(ostream& os, map<string,int> &m){
	for (const auto& p : m){
		cout << "\n"  << p.first << ": " << p.second << "\n";
	}
	return os;
}
ostream& operator<<(ostream& os, map<int,string> &m){
	for (const auto& p : m){
		cout << "\n" << p.first << ": " << p.second << "\n";
	}
	return os;
}

int main(){

	map<string, int> msi;

	msi["lecture"]=21;
	msi["car"]=38;
	msi["train"]=49;
	msi["soup"]=22;
	msi["battery"]=11;
	msi["monitor"]=54;
	msi["glass"]=12;
	msi["keyboard"]=19;
	msi["calculator"]=35;
	msi["bike"]=36;

	cout << msi;

	msi.erase(msi.begin(),msi.end());

	cout << msi;
	string s;
	int i;
	while (msi.size() != 10){
			cin >> s >> i;
			msi.insert(pair<string,int>(s,i));
	}

	cout << msi;

	cout << accumulate(msi.begin(),msi.end(),0,
			[](auto sum,auto &msi){return sum=msi.second+sum;})
			<< endl;

	map<int,string> mis;
	for (auto& x : msi){
		mis[x.second] = x.first;
	}

	cout << mis;

	return 0;
}
