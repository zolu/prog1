#include <iostream>
#include <list>
#include <vector>
#include <fstream>
#include <algorithm>
#include <iterator>
#include <numeric>
#include <string>
using namespace std;

template <typename T>
void print(T& t){
	for (auto i : t){
		cout << i << endl ;
	}
	cout << "----------------" << endl;
}

int main(){
	vector<double> vd;

	ifstream file;
	file.open("float.txt");

	for (double i ; file >> i;)vd.push_back(i);
	print(vd);

	vector<int> vi (vd.size());
	copy(vd.begin(), vd.end(), vi.begin());
	print(vi);

	for (int i = 0; i < vd.size(); ++i){
		cout << vd[i] << " " << vi[i] << "\n";
	}
	double vdSum=accumulate(vd.begin(), vd.end(),0.0000);
	double viSum=accumulate(vi.begin(), vi.end(),0.0000);
	cout << endl << "vdSum: " << vdSum << endl << endl;

	cout << endl << "vdSum-viSum: " << vdSum - viSum << endl;

	reverse(vd.begin(), vd.end());
	print(vd);

  double vdMean = vdSum/vd.size();
	cout << endl << "vdMean: " << vdMean << endl << endl;

	vector<double> vd2 (vd.size());
	copy_if(vd.begin(), vd.end(), vd2.begin(), [vdMean](auto& vd){return vd<vdMean;});

	print(vd2);

	sort(vd.begin(), vd.end());
	print(vd);
	return 0;
}
