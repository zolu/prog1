#include <iostream>
#include <list>
#include <algorithm>
#include <fstream>
#include <vector>
using namespace std;

struct Item {
	string name;
	int iid;
	double value;
};

istream& operator>>(istream& is, Item& i){
	string iname;
	int iiid;
	double ivalue;
	is >> iname >> iiid >> ivalue;
	Item ij {iname,iiid,ivalue};
	i = ij;
	return is;
}

ostream& operator<<(ostream& os, Item& i){
	return os << i.name << " " << i.iid << " "<< i.value << endl;
}

template<typename T>
void print(T& t)
{
    for (auto p = t.begin(); p != t.end(); ++p)
        cout<<*p;

		cout << endl;
}

int main(){

	Item it;
	vector <Item> vi;
	ifstream file;
	file.open("file.txt");

  for (Item it; file >> it; ) vi.push_back(it);

	file.close();

	print(vi);

	sort(vi.begin(),vi.end(), [](const Item& a, const Item& b)
			{return a.name < b.name; } );

	print(vi);

	sort(vi.begin(),vi.end(), [](const Item& a, const Item& b)
			{return a.iid < b.iid; } );

	print(vi);

	sort(vi.begin(),vi.end(), [](const Item& a, const Item& b)
			{return a.value > b.value; } );

	print(vi);

	Item horseShoe{"horse shoe", 99, 12.34};
	Item canon{"Canon S400", 9988, 499.95};

	vi.push_back(horseShoe);
	vi.push_back(canon);

	print(vi);

	vi.erase(find_if(vi.begin(),vi.end(),
		[](const Item& a){return a.name == "horse shoe";}));

	print(vi);

	vi.erase(find_if(vi.begin(),vi.end(),
		[](const Item& a){return a.iid== 9988;}));

	print(vi);

	file.open("file.txt");

	list<Item> li;
  for (Item it; file >> it; ) li.push_back(it);

	file.close();

	print(li);

	li.sort([](const Item& a, const Item& b)
			{return a.name < b.name; } );

	print(vi);

	li.sort([](const Item& a, const Item& b)
			{return a.iid < b.iid; } );

	print(vi);

	li.sort([](const Item& a, const Item& b)
			{return a.value > b.value; } );

	print(li);

	li.push_back(horseShoe);
	print(li);

	li.push_back(canon);
	print(li);

	li.erase(find_if(li.begin(),li.end(),
		[](const Item& a){return a.name == "horse shoe";}));

	print(li);

	li.erase(find_if(li.begin(),li.end(),
		[](const Item& a){return a.iid== 9988;}));

	print(li);

	return 0;
}
