/* g++ main.cpp Graph.cpp Window.cpp GUI.cpp Simple_window.cpp -o main `fltk-config --ldflags --use-images` -std=c++11 */
#include "Simple_window.h"
#include "Graph.h"

int main()
{
	try{
	Point tl{100,100};
	int width = 1000;
	int height = 800;
	Simple_window win{tl, width, height, "xd"};

	int size = 800;
	int grid_size = 100;
	int z = size/100;
	Lines grid;

	for (int x = grid_size; x <= size; x+=grid_size){
		grid.add(Point{x,0},Point{x,size});
		grid.add(Point{0,x},Point{size,x});
	}

	Image gyozike1{Point{600,0},"gyozike.jpeg"};
	gyozike1.set_mask(Point{100,0},200,200);

	Image gyozike2{Point{600,200},"gyozike.jpeg"};
	gyozike2.set_mask(Point{100,0},200,200);

	Image gyozike3{Point{400,200},"gyozike.jpeg"};
	gyozike3.set_mask(Point{100,0},200,200);

	Vector_ref<Rectangle> rx;
	for (int i = 0; i < z; i++){
		for (int j = 0; j < z; j++){
			if (i == j){
				rx.push_back(new Rectangle{Point{i*grid_size, j*grid_size},grid_size,grid_size});
				rx[i].set_fill_color(Color::red);
				win.attach(rx[i]);
			}

		}
	}

	Image gyozo{Point{0,0},"gyozike.jpeg"};
	gyozo.set_mask(Point{100,50},100,100);

	win.attach(grid);
	win.attach(gyozike1);
	win.attach(gyozike2);
	win.attach(gyozike3);
	win.attach(gyozo);


	for (int d = 0; d < z; ++d){
		for (int r = 0; r < z; ++r){
		win.wait_for_button();
		gyozo.move(100,0);
		}
		gyozo.move(-800,100);
	}
	win.wait_for_button();

	}
	catch (exception& e) {
		return 1;
	}
	catch (...) {
		return 2;
	}
}
