#include"std_lib_facilities.h"


template<typename T>
struct S
{
	S(T t) : val(t) {}

	const T& get() const {return val;};
	T& getnoc() {return val;};
	//T& set(T t) { val = t;};
	S& operator=(const T& va) {return val = va;};
	void readval(T& v);

	private:
		T val;
};

template<typename T>
const T& get(S<T>& s) 
{
	return s.get();
}

template<typename T>
T& getnoc(S<T>& s) 
{
	return s.getnoc();
}

template<typename T>
void readval(T& v)
{
	cin >> v;	
}


int main(){
	S<int> sint(2);
	S<char> schar('x');
	S<double> sdouble(1.23);
	S<vector<int>> svint (vector<int> {1,2,6});
	S<std::string> sstring("string");

	cout << get(sint) << endl;
	cout << getnoc(sint) << endl;
	cout << get(schar) << endl;
	cout << get(sdouble) << endl;
	cout << get(sstring) << endl;
		for(int i : get(svint))
				cout << i << " ";

	cout << endl;

	int rvint;
	readval(rvint);
	S<int> sintx(rvint);
	
	char rvchar;
	readval(rvchar);
	S<char> scharx(rvchar);

	double rvdouble;
	readval(rvdouble);
	S<double> sdoublex(rvdouble);

	string rvstring;
	readval(rvstring);
	S<string> sstringx(rvstring);

	return 0;
}
