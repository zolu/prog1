#include <stdio.h>

void func(char * p, int x){
    printf("p is %s and x is %d\n",p, x);
}

int main(){
    printf("hello world\n");

    char * h = "hello";
    char * w = "world";

    printf("%s %s\n",h,w);

    func("hello",88);
    
}